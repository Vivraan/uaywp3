function [NavData, json] = readNavDataById(id, path)
%%
%   Obtains saved Directions API response data from given file ID. The
%   extension is assumed to be '.json'.
%
%   Input : file ID, and optionally path where it exists
%
%   Output: Struct containing response, similar to a fresh call to
%   fetchNavigationData(), and the JSON data as a string.
%%
    global Strings
    
    id = char(id);
    if nargin == 1
        path = fullfile(pwd, Strings.Directories.saved, [id '.json']);
    else
        path = fullfile(path, [id '.json']);
    end
    json = fileread(path);
    NavData = jsondecode(json);
    
    % append the ID if it isn't already there.
    if ~isfield(NavData, 'id')
        NavData.id = id;
    end
end
