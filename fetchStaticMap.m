function savedImageFilePath = fetchStaticMap(NavData, mapType, forceNewMapRequest, shouldDisplayImage)
%%
%   Fetches static map image for given routes.
%
%   savedImageFilePath = fetchStaticMap(NavData, mapType, forceNewMapRequest, shouldDisplayImage)
%
    global Strings

    if nargin == 3
        shouldDisplayImage = false;
    end
    
    assertNavData(NavData)
    
    filePath = fullfile(pwd, Strings.Directories.saved, [NavData.id '.png']);
    
    % warn against forcing new requests
    if forceNewMapRequest
        warning('Forcing new Maps Static request. Incurs additional billing.')
    end
    
    if exist(filePath, 'file') == 2 && ~forceNewMapRequest
        warning('Image for specified response ID already exists.')
        savedImageFilePath = filePath;
    else
        % obtain as many colours as there are routes, randomly
        % thus, the maximum number of routes is fixed to the number of
        % colours in strings.json
        rng('shuffle');
        colours = Strings.Colours.used;
        nColours = length(colours);
        nRoutes = length(NavData.routes);
        nMax = min(nRoutes, nColours);
        selectedColours = string(colours(randperm(nColours, nMax)));

        % get data from polylines already present in navdata
        pathPolylines = getRoutePolylines(NavData).';
        
        % generate single uppercase letters as route labels
        routeLabels = ('A':char('A'-1 + nRoutes)).';

        % The routes and steps are marked the same colour. In this case, we
        % code in these parameters ourselves.
        
        % build the 'path' query parameter
        path = compose('&path=color:%s|enc:%s', selectedColours, pathPolylines);
        path = [path{:}];                                                   % concatenate all results in path into one char vector

        % build the 'markers' query parameter
        markerDescriptors = compose( ...
         '&markers=label:%s|color:%s', routeLabels, selectedColours ...
        );
        markers = buildMarkerQueryPlaceholder(NavData);
        markers = sprintf(markers, markerDescriptors{:});

        % check the 'maptype' parameter just in case
        if ~any(strcmp(Strings.MapsStaticTypes, mapType))
            mapType = 'roadmap';
        end
        
        baseUrl = Strings.ApiUrlStubs.mapsStatic;
        size = '?size=640x640';                                             % premium limit is 2048x2048 and its factors if scale is mentioned
        scale = '&scale=2';                                                 % 1, 2, and for premium plan, 4
        format = '&format=png32';                                           % png/png8(default)|png32|gif|jpg|jpg-baseline
        mapType = ['&maptype=' mapType];                                    % roadmap(default)|satellite|hybrid|terrain
        apiKey = ['&key=' Strings.key];

        url = [baseUrl size scale markers path format mapType apiKey];

        disp('Requesting static map image from Google Maps Static API...')
        % save the image with the same name as the Directions API response
        savedImageFilePath = websave(filePath, url);
    end

    if shouldDisplayImage
        web(['file:///' savedImageFilePath]);
    end
end

function query = buildMarkerQueryPlaceholder(NavData)
%%
%   Finds the middle-most location per route, and makes placeholder text
%   with this data.
%
    assertNavData(NavData)

    query = '';
    
    % to store the value of the previous location
    prevLocation = struct('lat', 0, 'lng', 0);
    
    for i = 1:length(NavData.routes)
        legs = NavData.routes(i).legs;
        steps = legs.steps;
        
        % Find the step that lies nearly halfway in the route
        distanceCovered = 0;
        middle = 1;

        while distanceCovered <= legs.distance.value * 0.5
            distanceCovered = distanceCovered + steps{middle}.distance.value;
            middle = middle + 1;
        end
        
        % if the step is quite large or the end is the same as last time
        largeMiddleStep = ...
            steps{middle}.distance.value > steps{middle-1}.distance.value;
        
        sameLocation = prevLocation.lat == steps{middle}.end_location.lat ...
            && prevLocation.lng == steps{middle}.end_location.lng;
        
        if middle > 1 && (largeMiddleStep || sameLocation)
            middle = middle - 1;
        end
        
        % use the end of that particular step
        prevLocation = steps{middle}.end_location;
        middleStepEnd = loc2str(prevLocation);
        
        query = [query '%s|' middleStepEnd];
    end
end
