function result = getRoutePolylines(NavData)
%%
%   Returns the encoded overview polyline of each route.
%
    routes = [NavData.routes];
    routes = [routes.overview_polyline];
    polylines = cell(1, length(routes));
    [polylines{:}] = routes.points;
    result = string(polylines);
end
