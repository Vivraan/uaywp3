function result = loc2str(location)
%%  Alias for makeLocationUrlSafe().
    result = makeLocationUrlSafe(location);
end

function result = makeLocationUrlSafe(location)
%%
%   Makes a URL-safe string location based on whether it's a coordinate or
%   a string query. Place IDs are unaffected since they do not contain
%   spaces. Returns an empty string otherwise.
%
    result = '';
    if iscell(location)
        location = location{1};
    end
    % if it's a plain char vector
    if ischar(location)
        result = strrep(location, ' ', '+');
    % if it's a floating point array
    elseif isfloat(location)
        result = num2str(location, '%f,%f');
    % if it's a struct that resembles Google's latlng
    elseif all(isfield(location, {'lat', 'lng'}))
        result = [num2str(location.lat, 16) ',' num2str(location.lng, 16)];
    else
        warning('Incorrect argument type. Result is an empty string.')
    end
end
