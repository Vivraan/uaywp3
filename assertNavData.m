function assertNavData(NavData)
%%
%   Asserts that the JSON response adheres to certain criteria.
%
    % There must be an error message and status must be 'OK'
    if isfield(NavData, 'error_message')
        assert(strcmp(NavData.status, 'OK'), ...
            'Status returned is %s, with message ''%s''', ...
            NavData.status, NavData.error_message)
    end
    
    % The start and end of the route must differ
    if ~iscell(NavData.geocoded_waypoints)
        startPlaceId = NavData.geocoded_waypoints(1).place_id;
        endPlaceId = NavData.geocoded_waypoints(2).place_id;   
    else
        startPlaceId = NavData.geocoded_waypoints{1}.place_id;
        endPlaceId = NavData.geocoded_waypoints{2}.place_id;
    end
    assert(~strcmp(startPlaceId, endPlaceId), 'Start and end locations are same!')
    
    % The response must contain the custom ID timestamp
    assert(any(contains(fieldnames(NavData), 'id')), [
                'NavData structure must have a field called ''id'' ' ...
                'containing the name of the JSON file.'
                ]);
end