function [ElevationData, GradeData] = computeRouteGrades(NavData, forceNewElevationsRequest, shouldPlotData)
%%
%   Computes grade for the path in each step generated per route from the
%   Google Maps API. Optionally plots data.
%
%   GradeData = computeRouteGrades(NavData, forceNewElevationsRequest, shouldPlotData)
%
    if nargin == 2
        shouldPlotData = false;
    end

    assertNavData(NavData)
    % The Map Toolbox is needed for using the 'distance' function for
    % calculating geodetic distance. Throw an error if not available.
    assert(logical(license('test', 'map_toolbox')))
    
    if shouldPlotData
        text = ['Grade data along path travelled - ' NavData.id];
        hFigure = figure('Name', text, 'NumberTitle', 'off');
        set(hFigure, 'Position', get(0, 'Screensize'))
    end

    ElevationData = fetchElevationData(NavData, forceNewElevationsRequest);
    
    k = 1;
    nRoutes = length(ElevationData);
    for i = 1:nRoutes
        nResults = length(ElevationData(i).results);
        GradeData.Routes(i).rises = zeros(1, nResults);
        GradeData.Routes(i).runs = zeros(1, nResults);
        GradeData.Routes(i).grades = zeros(1, nResults);

        dist = int32(0);
        for j = 2:nResults
            a = ElevationData(i).results(j-1);
            b = ElevationData(i).results(j);
            
            rise = abs(a.elevation - b.elevation);
            % retrieves geodetic distance considering the Earth follows the
            % WGS84 ellipsoid model - a really good approximation. This is
            % only available through the Maps Toolbox.
            run = distance( ...
                a.location.lat, ...
                a.location.lng, ...
                b.location.lat, ...
                b.location.lng, ...
                wgs84Ellipsoid ...
                ... % referenceEllipsoid('WGS84') ...
            );
            dist = dist + run;
            
            GradeData.Routes(i).distanceCovered(j) = dist;
            GradeData.Routes(i).rises(j) = rise;
            GradeData.Routes(i).runs(j) = run;
            GradeData.Routes(i).grades(j) = rise/run;
            k = k + 1;
        end

        if shouldPlotData
            routeLabel = char('A'-1+i);
            
            % plot grades per step
            subplot(2, nRoutes, i)
            plot(GradeData.Routes(i).distanceCovered, GradeData.Routes(i).grades)
            xlabel('distance covered (m)')
            ylabel('grade')
            title(['Grades over distance covered for route ' routeLabel]);
                      
            % plot rises to runs - dense clusters are good
            subplot(2, nRoutes, i + nRoutes)
            scatter(GradeData.Routes(i).runs, GradeData.Routes(i).rises)
            xlabel('run (m)')
            ylabel('rise (m)')
            title(['Rise to run ratios for route ' routeLabel]);
        end
    end
end
    