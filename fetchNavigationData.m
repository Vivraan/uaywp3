function [NavData, savedJsonFilePath, url] = fetchNavigationData(origin, destination, dateTime, shouldDisplayJson)
%%
%   Obtains data for one or more routes between regions provided by 
%   query strings from Google Map's Directions API, and saves it in a JSON
%   file.
%
%   Input : origin and destination location queries - char vectors
%
%   Output: structured JSON data obtained from Google Maps' Directions API,
%           request URL, path of json data on disk
%
    global Strings

    if nargin == 3
        shouldDisplayJson = false;
    end

    % convert spaces in query strings to plus signs, if these are not
    % floating point location values
    originQuery = loc2str(origin);
    destinationQuery = loc2str(destination);
    
    % create a unique response ID for the first request, which is a timestamp
    id = datestr(now, 'ddmmmyyyy_HH-MM-SS-FFF');
    
    % create a JSON file with this ID as the name
    filePath = fullfile(pwd, Strings.Directories.saved, [id '.json']);
    
    % build the URL manually
    baseUrl = Strings.ApiUrlStubs.directions;
    originQuery = ['?origin=' originQuery];
    destinationQuery = ['&destination=' destinationQuery];
    alternatives = '&alternatives=true'; ...                                 true/false
    avoid = '&avoid=indoor'; ...                                             tolls|highways|ferries|indoor
    transitRoutingPref = '&transit_routing_preference=less_walking'; ...     less_walking|fewer_transfers
    
    if strcmp(dateTime, 'now')
        departureTime = dateTime
    else
        departureTime = num2str(int64(posixtime(datetime(dateTime))))
    end

    departureTime = ['&departure_time=' departureTime]; ...
    trafficModel = '&traffic_model=best_guess'; ...                          best_guess|optimistic|pessimistic
    apiKey = ['&key=' Strings.key];
        
    url = [
        baseUrl ...
        originQuery destinationQuery ...
        alternatives avoid transitRoutingPref ...
        departureTime trafficModel ...
        apiKey ...
        ];
    
    disp('Requesting navigation data from Google Maps Directions API...')
    % read the fetched content into a struct
    savedJsonFilePath = websave(filePath, url);

    % store data for workspace use
    NavData = jsondecode(fileread(savedJsonFilePath));
        
    % append the ID as a field to the MATLAB structure
    NavData.id = id;

    if shouldDisplayJson
        web(['file:///' savedJsonFilePath]);
    end
end
