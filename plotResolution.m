function plotResolution(ElevationData, GradeData)
%%
%   A utility function for plotting the changes in resolutions provided by
%   the Elevations API, along with distance covered on each step.
%
%   plotResolution(ElevationData, GradeData)
%
    text = 'Resolution over path';
    figure('Name', text, 'NumberTitle', 'off')
    for i = 1:length(ElevationData)
        subplot(length(ElevationData), 1, i)
        plot([GradeData.Routes(i).distanceCovered], [ElevationData(i).results.resolution])
        xlabel('path travelled (m)')
        ylabel('resolution (m)')
        title([text ' for route ' char('A'-1+i)])
        grid
    end